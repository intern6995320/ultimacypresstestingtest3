describe('AccountDetails module', () => {
    beforeEach(() =>{

        cy.visit('https://ultima.com.np/account/')
        cy.get('#username').type('subediramesh144@gmail.com')
        cy.get('#password').type('dipakdhakal')
        cy.get('.woocommerce-form > .woocommerce-button').click()
        cy.get('.woocommerce-MyAccount-navigation-link--edit-account > a').click()
    })
    it('should now able to edit firstname from the Account Details', ()=>{
        //cy.get('.woocommerce-MyAccount-navigation-link--edit-account > a').click()
        cy.get('#account_first_name').clear()
        cy.get('#account_first_name').type('Dai')
        cy.get('.woocommerce-Button').click()
        cy.get('.woocommerce-message').should('be.visible')

    })
    it('should now able to edit displayname from the Account Details', ()=>{
        //cy.get('.woocommerce-MyAccount-navigation-link--edit-account > a').click()
        cy.get('#account_display_name').clear()
        cy.get('#account_display_name').type('Ramesh')
        cy.get('.woocommerce-Button').click()
        

    })
    it('should now able to edit firstname as previous from the Account Details', ()=>{
        //cy.get('.woocommerce-MyAccount-navigation-link--edit-account > a').click()
        cy.get('#account_first_name').clear()
        cy.get('#account_first_name').type('Ramesh')
        cy.get('.woocommerce-Button').click()
        cy.get('.woocommerce-message').should('be.visible')

    })
    it('should now able to edit password from the Account Details', ()=>{
        //cy.get('.woocommerce-MyAccount-navigation-link--edit-account > a').click()
        cy.get('#password_current').type('dipakdhakal')
        cy.get('#password_1').type('rameshsubedi')
        cy.get('#password_2').type('rameshsubedi')
        cy.get('.woocommerce-Button').click()
        cy.get('.woocommerce-message').should('be.visible')

    })
})