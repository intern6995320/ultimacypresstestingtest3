describe('Logout module', () => {
    beforeEach(() =>{

        cy.visit('https://ultima.com.np/account/')
        cy.get('#username').type('subediramesh144@gmail.com')
        cy.get('#password').type('dipakdhakal')
        cy.get('.woocommerce-form > .woocommerce-button').click()
    })
    
    it('Should now logout successfully', ()=>{
        cy.get('.woocommerce-MyAccount-navigation-link--customer-logout > a').click()
    })
})