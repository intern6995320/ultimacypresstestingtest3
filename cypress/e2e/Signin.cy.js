describe('Signin module', () => {
    beforeEach(() =>{

        cy.visit('https://ultima.com.np/account/')

    })
    
    it('should Login successfully', () => {
        cy.get('#username').type('subediramesh144@gmail.com')
        cy.get('#password').type('dipakdhakal')
        cy.get('.woocommerce-form > .woocommerce-button').click()
        cy.get('.woocommerce-MyAccount-navigation-link--customer-logout > a').should('be.visible')
    })
    it('Invalid Email', () => {
        cy.get('#username').type('subediramesh144gmail.com')
        cy.get('#password').type('dipakdhakal')
        cy.get('.woocommerce-form > .woocommerce-button').click()
        cy.get('.woocommerce-error > li').should('be.visible')

    })
    it('Invalid Password', () => {
        cy.get('#username').type('subediramesh144@gmail.com')
        cy.get('#password').type('dipakdha')
        cy.get('.woocommerce-form > .woocommerce-button').click()
        cy.get('.woocommerce-error > li').should('be.visible')
    })
    it('Empty email field', () => {
        cy.get('#password').type('dipakdhakal')
        cy.get('.woocommerce-form > .woocommerce-button').click()
        cy.get('.woocommerce-error > li').should('be.visible')
    })
    it('Empty password field', () => {
        cy.get('#username').type('subediramesh144@gmail.com')
        cy.get('.woocommerce-form > .woocommerce-button').click()
        cy.get('.woocommerce-error > li').should('be.visible')
    })
})