describe('sign up module', () => {
    beforeEach(() =>{

        cy.visit('https://ultima.com.np/account/')

    })
    
    it('should sign up successfully', () => {
        cy.get('#reg_billing_first_name').type('Ramesh')
        cy.get('#reg_billing_last_name').type('Subedi')
        cy.get('#reg_email').type('subediramesh144@gmail.com')
        cy.get('#reg_password').type('dipakdhakal')
        cy.get('.woocommerce-Button').click()
    })
    it('Invalid Email', () => {
        cy.get('#reg_billing_first_name').type('Ramesh')
        cy.get('#reg_billing_last_name').type('Subedi')
        cy.get('#reg_email').type('subediramesh144gmail.com')
        cy.get('#reg_password').type('dipakdhakal')
        cy.get('.woocommerce-Button').click()
        cy.get('#reg_email').should('be.visible')
    })
    it('Invalid Password', () => {
        cy.get('#reg_billing_first_name').type('Ramesh')
        cy.get('#reg_billing_last_name').type('Subedi')
        cy.get('#reg_email').type('subediramesh000000@gmail.com')
        cy.get('#reg_password').type('7')
        cy.get('.woocommerce-Button').click()
        cy.get('#reg_email').should('be.visible')
    })
    it('empty email field', () => {
        cy.get('#reg_billing_first_name').type('Dipak')
        cy.get('#reg_billing_last_name').type('Subedi')
        cy.get('#reg_password').type('dipaksubedi')
        cy.get('.woocommerce-Button').click()
        cy.get('#reg_email').should('be.visible')
    })
    it('empty password field', () => {
        cy.get('#reg_billing_first_name').type('Madhu')
        cy.get('#reg_billing_last_name').type('Kafle')
        cy.get('#reg_email').type('dhakaldipak286@gmail.com')
        cy.get('.woocommerce-Button').click()
        cy.get('#reg_email').should('be.visible')
    })
    it('Empty first name field', () => {
        cy.get('#reg_billing_last_name').type('Subedi')
        cy.get('#reg_email').type('subediramesh144@gmail.com')
        cy.get('#reg_password').type('dipakdhakal')
        cy.get('.woocommerce-Button').click()
        cy.get('#reg_email').should('be.visible')
    })
    it('Empty last name', () => {
        cy.get('#reg_billing_first_name').type('Bhumi')
        cy.get('#reg_email').type('madhukafle141@gmail.com')
        cy.get('#reg_password').type('madhudoxt')
        cy.get('.woocommerce-Button').click()
        cy.get('#reg_email').should('be.visible')
    })
  })
